package Assignment2;

import java.util.ArrayList;
import java.util.Random;

public class Simulation implements Runnable{
	private ArrayList<Client> clients;
	private ArrayList<Register> registers;
	private static int MAX_TIME_BETWEEN_CLIENTS=10;
	
	public Simulation() {
		GUI g = GUI.getGUI();
		int nrClients=g.getNrClients();
		int nrRegisters=g.getNrRegisters();
		int maxTimeClients=g.getMaxTimeClients();
		
		Client.setMaxTime(maxTimeClients);
		clients= new ArrayList<Client>();
		registers=new ArrayList<Register>();
		for (int i=0; i<nrClients; i++)
			clients.add(new Client());
		for (int i=0; i<nrRegisters; i++)
			registers.add(new Register());
	}
	

	public void run() {
		// TODO Auto-generated method stub
		for (Register r:registers)
			(new Thread(r)).start();
		
		Random r = new Random();
		for (Client c: clients)
		{
			int rt = r.nextInt(MAX_TIME_BETWEEN_CLIENTS);
			try {
				Thread.sleep(rt*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int ir = r.nextInt(registers.size());
			registers.get(ir).add(c);
		}
	}

}
