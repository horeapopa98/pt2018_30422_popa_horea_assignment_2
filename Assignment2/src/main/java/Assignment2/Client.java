package Assignment2;


public class Client {
	private static int ID_COUNT=0;
	private static int MAX_TIME;
	
	private int ID;
	private int time;
	
	public Client()
	{
		ID = ID_COUNT++;
		time = 1 + (int)(Math.random()*MAX_TIME); 
	}

	public int getID() {
		return ID;
	}

	public int getTime() {
		return time;
	}
	
	public String toString()
	{
		return "CLIENT: "+ID + " serving time: "+time;
	}
	
	public static void setMaxTime(int time)
	{
		MAX_TIME=time;
	}

}
