package Assignment2;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI {
	private static GUI g;
	public static GUI getGUI()
	{
		if (g==null)
			g=new GUI();
		return g;
	}
	
	private JTextField nrClients;
	private JTextField nrRegisters;
	private JTextField maxTimeClients;
	private JTextArea log;
	
	public int getNrClients()
	{
		return Integer.parseInt(nrClients.getText());
	}
	
	public int getNrRegisters()
	{
		return Integer.parseInt(nrRegisters.getText());
	}
	
	public int getMaxTimeClients()
	{
		return Integer.parseInt(maxTimeClients.getText());
	}
	
	public void logMessage(String msg)
	{
		log.setText(log.getText()+"\n"+msg);
	}

	private GUI()
	{
		JFrame frame=new JFrame("Shopping");
		frame.setSize(1000, 500);
		frame.setLayout(new FlowLayout());

		JPanel inputPanel = new JPanel();
		inputPanel.setSize(300,200);
		inputPanel.setLayout(new GridLayout(3, 2));
		inputPanel.add(new JLabel("Nr. clients:"));
		nrClients = new JTextField(10);
		nrClients.setText("10");
		inputPanel.add(nrClients);
		inputPanel.add(new JLabel("Nr. registers:"));
		nrRegisters = new JTextField(10);
		nrRegisters.setText("4");
		inputPanel.add(nrRegisters);
		inputPanel.add(new JLabel("Max. time clients:"));
		maxTimeClients = new JTextField(10);
		maxTimeClients.setText("10");
		inputPanel.add(maxTimeClients);
		
		frame.add(inputPanel);

		JButton startSim = new JButton ("START Simulation");
		frame.add(startSim);
		startSim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Simulation s = new Simulation();
				new Thread(s).start();
			}
		});

		log = new JTextArea(20,50);
		log.setEditable(false);
		JScrollPane scroll = new JScrollPane (log);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		frame.add(scroll);

		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
