package Assignment2;

import java.util.ArrayList;

public class Register implements Runnable{
	private static int ID_COUNT=0;
	private int ID;
	private ArrayList<Client> queue;
	
	public Register()
	{
		ID=ID_COUNT++;
		queue = new ArrayList<Client>();
	}
	
	public synchronized void add(Client c)
	{
		queue.add(c);
		GUI.getGUI().logMessage(c + " arrived at REGISTER "+ID);
		notify();
	}
	
	public synchronized void removeFirst()
	{
		queue.remove(0);
	}
	
	public Client getFirst()
	{
		return queue.get(0);
	}
	
	public String toString()
	{
		String s="REGISTER "+ID+" clients in the queue: ";
		for (Client c:queue)
			s+=c.getID()+" ";
		return s;
	}
	
	public void run() { 
		// TODO Auto-generated method stub
		while(true)
		{
			if (queue.size()==0)
				synchronized(this) {
					try {
						this.wait();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			else
			{
				try {
					Thread.sleep(getFirst().getTime()*1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				GUI.getGUI().logMessage(queue.get(0)+" is served!!!");
				removeFirst();
				GUI.getGUI().logMessage(this.toString());
			}
		}
	}
}